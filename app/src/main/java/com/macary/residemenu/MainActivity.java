package com.macary.residemenu;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.macary.residemenu.library.ResideMenu;
import com.macary.residemenu.library.ResideMenuItem;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnResideMenu = findViewById(R.id.btn_reside_menu);

        /**
         * Creación del menu con el efecto.*/
        ResideMenu resideMenu = new ResideMenu(this, R.layout.header_menu, -1);
        resideMenu.setUse3D(true);
        resideMenu.setBackground(R.drawable.fondo_azul_side);
        resideMenu.attachToActivity(this);
        resideMenu.setSwipeDirectionDisable(ResideMenu.DIRECTION_LEFT);
        resideMenu.setSwipeDirectionDisable(ResideMenu.DIRECTION_RIGHT);
        resideMenu.setScaleValue(0.6f);

        /**Acceso a las vistas colocadas en el header del menu.*/
        ImageView imageView = resideMenu.getLeftMenuView().findViewById(R.id.imageView);
        TextView textView = resideMenu.getLeftMenuView().findViewById(R.id.textView);

        imageView.setImageResource(R.mipmap.ic_launcher);
        textView.setText("android.studio@android.com");

        /**
         * Items del menu.*/
        ResideMenuItem item1 = new ResideMenuItem(this, R.drawable.ic_launcher_foreground, "item 1");
        ResideMenuItem item2 = new ResideMenuItem(this, R.drawable.ic_launcher_foreground, "item 2");
        ResideMenuItem item3 = new ResideMenuItem(this, R.drawable.ic_launcher_foreground, "item 3");

        /**
         * Eventos click de las opciones del menu.*/
        item1.setOnClickListener(v -> resideMenu.closeMenu());
        item2.setOnClickListener(v -> resideMenu.closeMenu());
        item3.setOnClickListener(v -> resideMenu.closeMenu());

        /**
         * Adición y seteo de la posición de las opciones en el menu.*/
        resideMenu.addMenuItem(item1, ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(item2, ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(item3, ResideMenu.DIRECTION_LEFT);

        /**
         * Evento click para abrir el menu al lado izquierdo de la pantalla.*/
        btnResideMenu.setOnClickListener(v -> resideMenu.openMenu(ResideMenu.DIRECTION_LEFT));
    }
}